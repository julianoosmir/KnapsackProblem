import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
public class App {

    public static void main(String[] args) throws Exception {
        List<Integer> numeros = Arrays.asList(300,100,200,9,8,6,2,1,3,5);

        int result = knapsack(305,numeros);
        System.out.println("resultado : " + result);
    }
    static Integer knapsack(int soma, List<Integer> numeros){
       
        int total = numeros.stream().reduce(0,(x,y)-> x + y);
        if(soma > total){
            System.out.println("numeros insuficiente para soma ");
            return 0;
        }

        int valor = 0;
        List<Integer> numeroSomados = new ArrayList<>();

        for(int numero : numeros){
            valor += numero;
            numeroSomados.add(numero);
            if(valor == soma){
                numeroSomados.forEach(x -> System.out.println("numeros somados: " + x ));
                return valor;
            }
        }
        Collections.shuffle(numeros);
        return knapsack(soma, numeros);
    }
}
